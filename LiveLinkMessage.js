const uuid = require('uuid');
const struct = require('python-struct');
const Timecode = require('smpte-timecode')

class LiveLinkMessage {

    constructor(mediapipeMessage) {
        this.version = 6
        this.uuid = "$" + uuid.v1()
        this.name = "RafMediapipe1"
        this.fps = 60
        this.subFrame = 1056060032
        this.denominator = parseInt(this.fps / 60)

    }

    createLiveLinkMessage() {
        
        let totalLength = 0

        //version
        let versionBuffer = struct.pack('<I', this.version);
        let versionBytes = Uint8Array.from(versionBuffer)
        totalLength += versionBytes.length

        //uuid
        let uuidBytes = new TextEncoder().encode(this.uuid)
        totalLength += uuidBytes.length

        //name
        let nameLenghtBytes = struct.pack('>i',this.name.length)
        totalLength += nameLenghtBytes.length
        let nameBytes = new TextEncoder().encode(this.name)
        totalLength += nameBytes.length


        //frames
        let timecode = new Timecode(new Date(),this.fps)
        let framesBytes = struct.pack('>II', timecode.frameCount, this.subFrame)
        totalLength += framesBytes.length

        //frame rate
        let frameRateBytes = struct.pack('>II', this.fps, this.denominator)
        totalLength += frameRateBytes.length


        let fullBytes = new Uint8Array(totalLength + nameLenghtBytes.length)


        let offset = 0
        fullBytes.set(versionBytes)
        offset += versionBytes.length

        fullBytes.set(uuidBytes, offset)
        offset += uuidBytes.length

        fullBytes.set(nameLenghtBytes, offset)
        offset += nameLenghtBytes.length

        fullBytes.set(nameBytes,offset)
        offset += nameBytes.length

        fullBytes.set(framesBytes, offset)
        offset += framesBytes.length

        fullBytes.set(frameRateBytes, offset)
        offset += frameRateBytes.length

        fullBytes.set(nameLenghtBytes,offset)
        return fullBytes
        
        
    }
}

module.exports = {LiveLinkMessage}