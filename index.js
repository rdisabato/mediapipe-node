const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);

const { Server } = require("socket.io");
const io = new Server(server);

const { LiveLinkMessage } = require('./LiveLinkMessage');


//UDP SERVER EMULATING THE LIVE LINK SERVER
var UDP_PORT = 11111
var UDP_HOST = '127.0.0.1'
var dgram = require('dgram');
var udpServer = dgram.createSocket('udp4')



app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });



io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('data', (msg) => {
        console.log(JSON.stringify(msg))

      });
});

app.get('/test', (req, res) => {
    let msg = new LiveLinkMessage()
    let str = msg.createLiveLinkMessage()
    udpServer.send(str,0,str.length,11112,"127.0.0.1")
    res.send()
  });

server.listen(3000, () => {
  console.log('listening on *:3000');
});




udpServer.on('listening', function () {
    var address = udpServer.address()
    console.log('UDP Server listening on ' + address.address + ":" + address.port)
})
udpServer.on('message', function (message, remote) {
    console.log(remote.address + ':' + remote.port +' - ' + message)
 
})
udpServer.bind(UDP_PORT, UDP_HOST);